﻿namespace TicTacToeNEWGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.box1 = new System.Windows.Forms.Button();
            this.box2 = new System.Windows.Forms.Button();
            this.box3 = new System.Windows.Forms.Button();
            this.box4 = new System.Windows.Forms.Button();
            this.box5 = new System.Windows.Forms.Button();
            this.box6 = new System.Windows.Forms.Button();
            this.box7 = new System.Windows.Forms.Button();
            this.box8 = new System.Windows.Forms.Button();
            this.box9 = new System.Windows.Forms.Button();
            this.NewGamebtn = new System.Windows.Forms.Button();
            this.Resetbtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lbl_X_Won = new System.Windows.Forms.Label();
            this.lbl_O_Win = new System.Windows.Forms.Label();
            this.lblDraw = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // box1
            // 
            this.box1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box1.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box1.Location = new System.Drawing.Point(288, 143);
            this.box1.Name = "box1";
            this.box1.Size = new System.Drawing.Size(110, 110);
            this.box1.TabIndex = 0;
            this.box1.UseVisualStyleBackColor = false;
            this.box1.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box2
            // 
            this.box2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box2.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box2.Location = new System.Drawing.Point(404, 143);
            this.box2.Name = "box2";
            this.box2.Size = new System.Drawing.Size(110, 110);
            this.box2.TabIndex = 0;
            this.box2.UseVisualStyleBackColor = false;
            this.box2.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box3
            // 
            this.box3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box3.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box3.Location = new System.Drawing.Point(520, 143);
            this.box3.Name = "box3";
            this.box3.Size = new System.Drawing.Size(110, 110);
            this.box3.TabIndex = 0;
            this.box3.UseVisualStyleBackColor = false;
            this.box3.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box4
            // 
            this.box4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box4.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box4.Location = new System.Drawing.Point(288, 266);
            this.box4.Margin = new System.Windows.Forms.Padding(10);
            this.box4.Name = "box4";
            this.box4.Size = new System.Drawing.Size(110, 110);
            this.box4.TabIndex = 0;
            this.box4.UseVisualStyleBackColor = false;
            this.box4.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box5
            // 
            this.box5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box5.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box5.Location = new System.Drawing.Point(404, 266);
            this.box5.Name = "box5";
            this.box5.Size = new System.Drawing.Size(110, 110);
            this.box5.TabIndex = 0;
            this.box5.UseVisualStyleBackColor = false;
            this.box5.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box6
            // 
            this.box6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box6.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box6.Location = new System.Drawing.Point(520, 266);
            this.box6.Name = "box6";
            this.box6.Size = new System.Drawing.Size(110, 110);
            this.box6.TabIndex = 0;
            this.box6.UseVisualStyleBackColor = false;
            this.box6.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box7
            // 
            this.box7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box7.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box7.Location = new System.Drawing.Point(288, 389);
            this.box7.Name = "box7";
            this.box7.Size = new System.Drawing.Size(110, 110);
            this.box7.TabIndex = 0;
            this.box7.UseVisualStyleBackColor = false;
            this.box7.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box8
            // 
            this.box8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box8.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box8.Location = new System.Drawing.Point(404, 389);
            this.box8.Name = "box8";
            this.box8.Size = new System.Drawing.Size(110, 110);
            this.box8.TabIndex = 0;
            this.box8.UseVisualStyleBackColor = false;
            this.box8.Click += new System.EventHandler(this.SelectedBox);
            // 
            // box9
            // 
            this.box9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box9.Font = new System.Drawing.Font("Arial Narrow", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box9.Location = new System.Drawing.Point(520, 389);
            this.box9.Name = "box9";
            this.box9.Size = new System.Drawing.Size(110, 110);
            this.box9.TabIndex = 0;
            this.box9.UseVisualStyleBackColor = false;
            this.box9.Click += new System.EventHandler(this.SelectedBox);
            // 
            // NewGamebtn
            // 
            this.NewGamebtn.BackColor = System.Drawing.Color.Khaki;
            this.NewGamebtn.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewGamebtn.Location = new System.Drawing.Point(26, 210);
            this.NewGamebtn.Name = "NewGamebtn";
            this.NewGamebtn.Size = new System.Drawing.Size(152, 66);
            this.NewGamebtn.TabIndex = 1;
            this.NewGamebtn.Text = "NEW GAME";
            this.NewGamebtn.UseVisualStyleBackColor = false;
            this.NewGamebtn.Click += new System.EventHandler(this.NewGamebtn_Click);
            // 
            // Resetbtn
            // 
            this.Resetbtn.BackColor = System.Drawing.Color.Khaki;
            this.Resetbtn.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Resetbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Resetbtn.Location = new System.Drawing.Point(26, 313);
            this.Resetbtn.Name = "Resetbtn";
            this.Resetbtn.Size = new System.Drawing.Size(152, 57);
            this.Resetbtn.TabIndex = 2;
            this.Resetbtn.Text = "RESET";
            this.Resetbtn.UseVisualStyleBackColor = false;
            this.Resetbtn.Click += new System.EventHandler(this.Resetbtn_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightGreen;
            this.button3.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(772, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(118, 44);
            this.button3.TabIndex = 3;
            this.button3.Text = "EXIT";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lbl_X_Won
            // 
            this.lbl_X_Won.AutoSize = true;
            this.lbl_X_Won.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_X_Won.Location = new System.Drawing.Point(724, 210);
            this.lbl_X_Won.Name = "lbl_X_Won";
            this.lbl_X_Won.Size = new System.Drawing.Size(85, 26);
            this.lbl_X_Won.TabIndex = 4;
            this.lbl_X_Won.Text = "X-WON";
            // 
            // lbl_O_Win
            // 
            this.lbl_O_Win.AutoSize = true;
            this.lbl_O_Win.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_O_Win.Location = new System.Drawing.Point(731, 288);
            this.lbl_O_Win.Name = "lbl_O_Win";
            this.lbl_O_Win.Size = new System.Drawing.Size(83, 26);
            this.lbl_O_Win.TabIndex = 4;
            this.lbl_O_Win.Text = "O-Won";
            // 
            // lblDraw
            // 
            this.lblDraw.AutoSize = true;
            this.lblDraw.Font = new System.Drawing.Font("Algerian", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDraw.Location = new System.Drawing.Point(731, 374);
            this.lblDraw.Name = "lblDraw";
            this.lblDraw.Size = new System.Drawing.Size(78, 26);
            this.lblDraw.TabIndex = 4;
            this.lblDraw.Text = "DRAW";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Coral;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(323, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 46);
            this.label1.TabIndex = 5;
            this.label1.Text = "TIC TAC TOE";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.ClientSize = new System.Drawing.Size(977, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDraw);
            this.Controls.Add(this.lbl_O_Win);
            this.Controls.Add(this.lbl_X_Won);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Resetbtn);
            this.Controls.Add(this.NewGamebtn);
            this.Controls.Add(this.box9);
            this.Controls.Add(this.box6);
            this.Controls.Add(this.box3);
            this.Controls.Add(this.box8);
            this.Controls.Add(this.box7);
            this.Controls.Add(this.box5);
            this.Controls.Add(this.box4);
            this.Controls.Add(this.box2);
            this.Controls.Add(this.box1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button box1;
        private System.Windows.Forms.Button box2;
        private System.Windows.Forms.Button box3;
        private System.Windows.Forms.Button box4;
        private System.Windows.Forms.Button box5;
        private System.Windows.Forms.Button box6;
        private System.Windows.Forms.Button box7;
        private System.Windows.Forms.Button box8;
        private System.Windows.Forms.Button box9;
        private System.Windows.Forms.Button NewGamebtn;
        private System.Windows.Forms.Button Resetbtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lbl_X_Won;
        private System.Windows.Forms.Label lbl_O_Win;
        private System.Windows.Forms.Label lblDraw;
        private System.Windows.Forms.Label label1;
    }
}

