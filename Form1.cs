﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeNEWGame
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int player = 1;              //To display X and O on the buttons
        public int turn = 0;                // To count number of turns   
        public int Count_X_Won = 0;         // To count number of times "X" won 
        public int count_O_Won = 0;         // To count number of times "O" won 
        public int Count_Draw = 0;          // To count number of Draws


        private void SelectedBox(object sender, EventArgs e)
        {
            //Common button for all the 9 buttons in game where user will input "X" or"O" 

            Button button = (Button)sender;


            if (player % 2 == 0)
            {
                button.Text = "X";
                player++;
                turn++;
            }
            else
            {
                button.Text = "O";
                player++;
                turn++;
            }

            // Match Draw Condition
            if (Draw())
            {
                MessageBox.Show("MATCH DRAWN");
                Count_Draw++;
                NewMatch();
            }

            //Match Win Condition for "X" and "O"
            if (Winner())
            {
                if (button.Text == "X")
                {
                    MessageBox.Show("X WON");
                    Count_X_Won++;
                    NewMatch();

                }
                else
                {
                    MessageBox.Show("O WON");
                    count_O_Won++;
                    NewMatch();
                }
            }
        }


        //Function to check  Winner using all Possible conditions
        public bool Winner()
        {
        
            //Coulmn1
           if ((box1.Text == box4.Text) && (box1.Text == box7.Text) && box1.Text != "")
            {
                box1.BackColor = box4.BackColor = box7.BackColor = Color.Teal; ;

                return true;
            }

            // Coulmn 2
            else if ((box2.Text == box5.Text) && (box2.Text == box8.Text) && box2.Text != "")
            {
                box2.BackColor = box5.BackColor = box8.BackColor = Color.Violet;

                return true;
            }

            //Coulmn 3
            else if ((box3.Text == box6.Text) && (box3.Text == box9.Text) && box3.Text != "")
            {
                box3.BackColor = box6.BackColor = box9.BackColor = Color.SkyBlue;

                return true;
            }

            // ROW 1
            if ((box1.Text == box2.Text) && (box1.Text == box3.Text) && box1.Text != "")
            {
                box1.BackColor = box2.BackColor = box3.BackColor = Color.Red;

                return true;
            }

            //ROW 2
            else if ((box4.Text == box5.Text) && (box4.Text == box6.Text) && box4.Text != "")
            {

                box4.BackColor = box5.BackColor = box6.BackColor = Color.Yellow;
                return true;
            }

            //ROW 3
            else if ((box7.Text == box8.Text) && (box7.Text == box9.Text) && box7.Text != "")
            {
                box7.BackColor = box8.BackColor = box9.BackColor = Color.SteelBlue;
                return true;
            }

             // Diagnol 1
            if ((box1.Text == box5.Text) && (box1.Text == box9.Text) && box1.Text != "")
            {
                box1.BackColor = box5.BackColor = box9.BackColor = Color.RosyBrown;

                return true;
            }

            //Diagnol 2
            else if ((box3.Text == box5.Text) && (box3.Text == box7.Text) && box3.Text != "")
            {
                box3.BackColor = box5.BackColor = box7.BackColor = Color.PapayaWhip;

                return true;
            }

            else
            {
                return false;
            }
        }

        //Function to check Draw.
        public bool Draw()
        {
            if (turn == 9 && Winner() == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lbl_X_Won.Text = "X: " + Count_X_Won;
            lbl_O_Win.Text = "O: " + count_O_Won;
            lblDraw.Text = "Draws: " + Count_Draw;
        }

        
        // Function to apply whenever Newmatch will be started
        public void NewMatch()
        {
            player = 0;
            turn = 0;
            box1.Text = "";
            box2.Text = "";
            box3.Text = "";
            box4.Text = "";
            box5.Text = "";
            box6.Text = "";
            box7.Text = "";
            box8.Text = "";
            box9.Text = "";

            box1.BackColor = Color.WhiteSmoke;
            box2.BackColor = Color.WhiteSmoke;
            box3.BackColor = Color.WhiteSmoke;
            box4.BackColor = Color.WhiteSmoke;
            box5.BackColor = Color.WhiteSmoke;
            box6.BackColor = Color.WhiteSmoke;
            box7.BackColor = Color.WhiteSmoke;
            box8.BackColor = Color.WhiteSmoke;
            box9.BackColor = Color.WhiteSmoke;

            lbl_X_Won.BackColor = Color.OldLace;
            lbl_O_Win.BackColor = Color.OldLace;
            lblDraw.BackColor   = Color.OldLace;

            lbl_X_Won.Text = "X: " + Count_X_Won;
            lbl_O_Win.Text = "O: " + count_O_Won;
            lblDraw.Text = "Draws: " + Count_Draw;
        }
        private void NewGamebtn_Click(object sender, EventArgs e)
        {
            NewMatch();  //NewMatch will be called wheever user will click on NewGame button
        }

        private void Resetbtn_Click(object sender, EventArgs e)
        {
            Count_X_Won = 0;
            count_O_Won = 0;
            Count_Draw = 0;

            NewMatch();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit(); //Game will be exit
        }
    }
}
